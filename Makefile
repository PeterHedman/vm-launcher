_image = 'obald/vm-launcher'
_version = '0.1.1'
_time = $(shell date +"%Y%m%d_%H%M%S")

.PHONY: build
build:
	docker build --no-cache --tag $(_image):$(_version) .
	docker tag $(_image):$(_version) $(_image):latest

.PHONY: clean
clean:
	docker rmi $(_image):$(_version) $(_image):latest

.PHONY: start-test
start-test:
	docker run -itd --privileged -e VM_USER='tester' -e VM_PW='tester' -v $(PWD)/vm-launcher-test:/image --name test-vm-launcher $(_image):$(_version)

.PHONY: start-test-added-disk
start-test-added-disk:
	docker run -itd --privileged -e VM_USER='tester' -e VM_PW='tester' -e EXTRA_STORAGE_DEV_NAME='disk_dev2' -e EXTRA_STORAGE_DEV_SIZE="100G" -v $(PWD)/vm-launcher-test:/image --name test-vm-launcher $(_image):$(_version)

.PHONY: start-test-gui
start-test-gui:
	docker run -itd --privileged -e AUTO_ATTACH='N' -e SPICE_PORT='3001' -e VM_USER='tester' -e VM_PW='tester' -v $(PWD)/vm-launcher-test:/image --name test-vm-launcher-gui $(_image):$(_version)
	@echo 'for display remote-manager spice://<container_ip>:3001'

.PHONY: stop_test
stop-test:
	docker stop test-vm-launcher
	docker rm test-vm-launcher
	@echo '$(PWD)/vm-launcher-test needs to be removed as root'

.PHONY: stop-test-gui
stop-test-gui:
	docker stop test-vm-launcher-gui
	docker rm test-vm-launcher-gui
	@echo '$(PWD)/vm-launcher-test-gui needs to be removed as root'

.PHONY: push
push:
	docker tag $(_image):$(_version) $(_image):$(_version)-$(_time)
	docker push $(_image):$(_version)
	docker push $(_image):latest
	docker push $(_image):$(_version)-$(_time)

.PHONY: help
help:
	@echo 'Usage'
	@echo '  make build      : Builds vm-launcher container'
	@echo '  make clean      : Remove built vm-launcher container'
	@echo '  make start-test : Starts a test-vm'
	@echo '  make stop-test  : Stops and removes test-vm'
	@echo '  make push       : Push built docker images'
	@echo '  make help       : Prints this message'
