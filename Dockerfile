FROM alpine:3.14

RUN apk add --no-cache bash file qemu-system-x86_64 qemu-img ovmf bridge-utils pciutils iproute2 dnsmasq gzip wget cdrkit openssl qemu-modules

RUN mkdir vmimg

COPY LICENSE-2.0.txt /usr/local/LICENSE-2.0.txt

COPY spawnvm /usr/local/bin/spawnvm

RUN chmod u+x /usr/local/bin/spawnvm

VOLUME /image

ENTRYPOINT ["/usr/local/bin/spawnvm"]

