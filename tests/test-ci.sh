#!/bin/bash

#: ${VM_URL:="http://172.17.0.1:3000/focal-server-cloudimg-amd64-disk-kvm.img"}
: ${VM_URL:="http://172.17.0.1:3000/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"}
#: ${VM_URL:="https://ftp.acc.umu.se/mirror/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"}
#: ${VM_URL:="https://download.fedoraproject.org/pub/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.1.x86_64.qcow2"}
#: ${VM_URL:="https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2"}
#: ${VM_URL:="http://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"}
: ${VM_PUBLIC_KEY:="$(cat ~/.ssh/id_rsa.pub)"}

: ${LOCK_PW:='false'}
: ${ALLOW_PW:='True'}
#: ${PWHASH:='$6$blVjiO5qZFk3cX$j5.TqvhqaEYVcy3d5x1i/2X4TMIswdfONpe/SbnlSEDjxn3eWDtijoDD.suwfb1gYTP/W7SkK.OZK1FlqbXQ40'}
: ${PWHASH:="$(openssl passwd -1 -salt xyz admin)"}
: ${TEST_REMOTE_IMAGE:='true'}

export VM_URL
export VM_PUBLIC_KEY
export RAW_USER=$(cat <<EOF
#cloud-config
users:
  - default
  - name: peter
    passwd: $PWHASH
    groups: sudo
    lock_passwd: $LOCK_PW
    ssh_pwauth: $ALLOW_PW
    shell: /bin/bash
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    chpasswd: {expire: False}
    ssh_authorized_keys:
      - $VM_PUBLIC_KEY
runcmd:
  - sudo sed -i 's/127.0.0.1 localhost/127.0.0.1 localhost localhost.localdomain/g' /etc/hosts
final_message: "vm-container finally up after $UPTIME"
EOF
)

export RAW_META=$(cat <<EOF
{
  "availability_zone": "",
  "hostname": "vm-base-test",
  "uuid": "12345678"
}
EOF
)

echo "$RAW_META"

echo "$RAW_USER"

#echo "docker run -itd --privileged -v $(pwd)/vm-launcher-test:/image -e CLOUD_INIT_TYPE='RAW_config' -e VM_URL -e RAW_META -e AUTO_ATTACH='yes' -e RAW_USER -e VM_PUBLIC_KEY --name=vm-launcher-test vm-launcher:0.0.1"

if [[ "$TEST_REMOTE_IMAGE" == "true" ]]; then
  docker run -itd --privileged -v $(pwd)/vm-launcher-test:/image -e USE_NET_BRIDGES='0' -e CLOUD_INIT_TYPE='RAW_config' -e CPU_COUNT='1' -e MEMORY='2048' -e VM_URL -e RAW_META -e AUTO_ATTACH='yes' -e RAW_USER -e VM_PUBLIC_KEY -e EXTRA_DISK='10G' --name=vm-launcher-test obald/vm-launcher:latest
else
  : ${REMOTE_IMAGE:='false'}
  mkdir vm-launcher-test2/
  wget $VM_URL -O vm-launcher-test2/image
  sudo qemu-img resize vm-launcher-test2/image +30G
  docker run -itd --privileged -v $(pwd)/vm-launcher-test2:/image -e USE_NET_BRIDGES='0' -e CLOUD_INIT_TYPE='RAW_config' -e REMOTE_IMAGE='false' -e RAW_META -e AUTO_ATTACH='1' -e RAW_USER -e VM_PUBLIC_KEY --name=vm-launcher-test-local obald/vm-launcher:latest
fi
