# Hypervisor-container

## Overview

This is a docker image that contains hypervisor SW like qemu and bridge-utils etc.
It is meant to start and run a VM image in the context of a container.
Its main use is to provide a simple testing environment in short time,
But could also be used to run legacy VM's on modern CAAS platforms like kubernetes

This container is based on BBVA https://github.com/BBVA/kvm

And has been enhanced to support cloud-init and different network solution to be deployable
on kubernetes, see https://gitlab.com/PeterHedman/vm-launcher-helm. 
It is also able to utilize additional networks added via multus on a kubernetes cluster.

## Usage

To run this efficently it needs to be priviliged to access KVM/VT-x virtualization

```
docker run -itd --privileged -v $(pwd)/my-test-vm:/image -e VM_USER="peter" -e VM_PW="secret" -e VM_PUBLIC_KEY="$(cat ~/.ssh/id_rsa.pub)" --name=my-test-vm obald/vm-launcher
```

## Subset of Configurable settings

Below values can be altered via docker env variables

URL to vm image to use

- VM_URL:='http://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img'

VM memory and vcpu options

- CPU_COUNT:=1
- MEMORY:=1024

For more control one can set topology instead

- KVM_CPU_OPTS:="-m 1024 -smp 2,sockets=1,cores=1,threads=1"

If one wants to extend the size of the vm image file new size can be specified
valid values are nr followed by G for GB, ex '10G'

- EXTRA_DISK:='0'

If one wants to add an additonal storage device set below parameters

- EXTRA_STORAGE_DEV_NAME:="unset"
- EXTRA_STORAGE_DEV_SIZE:="unset"
- EXTRA_STORAGE_DEV_FORMAT:="raw"

Which type of cloud init to use either no_cloud, generic_cloud (same as config_drive) and
 RAW_cloud. 

- CLOUD_INIT_TYPE:='generic_cloud'

If RAW_cloud is used specify full contents of cloud-init files as 
here docs in below variables

- RAW_META:=""
- RAW_USER:="" 

If using generic or no cloud use below vars to change cloud init settings

Hostname for vm

- VM_NAME:="vm-test"

ssh key to add for users, ex $(cat ~/.ssh/id_rsa.pub) 

- VM_PUBLIC_KEY:=""

User to be added to vm

- VM_USER:="vmuser"

PW for user

- VM_PW:="secret"

Allow PW

- VM_ALLOW_PW:='true'

If one wants to use a local vm image instead of a remote one set below var to false, 
Observe that if an image named image is available in containers image dir no new image
 is downloaded no matter what this value is set to

- REMOTE_IMAGE:='true'

Boot with UEFI instead of legacy bios

- UEFI_BOOT:='N'

Allow nested virtualization

- NESTED_HVM='N'

Set DEBUG flag to get more printouts in shell

- DEBUG:='N'

Set USE_NET_BRIDGES if one should use macvlan '0' or normal linux bridges '1' 
for networking

- USE_NET_BRIDGES:='0'

DISABLE DHCP if wanted set below to 'N'

- ENABLE_DHCP:='Y'

To disable VGA set below to Y

- DISABLE_VGA:='N'

To enable remote GUI access set SPICE_PORT (3001)

- SPICE_PORT:='N'

To avoid VM to automatically connect to all container If set below to N

- AUTO_ATTACH:='Y'

Change block options

- KVM_BLK_OPTS:="-drive if=virtio,file=/image/image,format=$IMAGE_FORMAT"

### Network

If you want a private network instead of docker default

```
sudo brctl addbr myOwnNet && \
sudo brctl addif myOwnNet <interface> && \
sudo brctl setfd myOwnNet && \
ifconfig myOwnNet 10.0.190.0 netmask 255.255.0.0
```

Then connect VM-container to the network

```
docker network create --driver=bridge --ip-range=10.0.190.0.34 --subnet=10.0.0.0/16 --aux-address='ip1=10.0.190.1' --aux-address='ip2=10.0.190.2' --aux-address='ip3=10.0.190.3' -o "com.docker.network.bridge.name=myOwnNet" kubenet
```

And then start it

```
docker run -itd --privileged --network kubenet -v $(pwd)/my-test-vm:/image --name=<wanted-name> obald/vm-launcher
```

#### Multiple Networks

If one wants to create more than one network for the container that is also possible 

```
docker network create --driver=bridge network1 --subnet=172.19.0.0.34

docker network create --driver=bridge network2 --subnet=172.19.2.0/24

docker create --privileged --network network1 -v $(pwd)/my-test-vm:/image --name=<wanted-name> obald/vm-launcher

docker network connect network2 <wanted-name>

docker start <wanted-name> 
```

## Example image URL'S
fedora28 https://download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2
fedora27 https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2
fedora34 https://download.fedoraproject.org/pub/fedora/linux/releases/34/Cloud/x86_64/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2


CentOS7 http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1808.qcow2

Debian9 http://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd65.qcow2

openSUSE http://download.opensuse.org/repositories/Cloud:/Images:/Leap_15.0/images/openSUSE-Leap-15.0-OpenStack.x86_64-0.0.4-Buildlp150.12.25.qcow2

Ubuntu Xenial http://cloud-images.ubuntu.com/xenial/20180921/xenial-server-cloudimg-amd64-disk1.img
Ubuntu Xenial http://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-disk1.img
Ubuntu 20.04: https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-disk-kvm.img
Ubuntu Ermine http://cloud-images.ubuntu.com/ermine/20191017/ermine-cloudimg-amd64-disk1.img
